#include <stdio.h>
#include <stdlib.h>
#include "checksum.h"
#include "MurmurHash3.h"

int hammingDistance(uint64_t n1, uint64_t n2)
{
    uint64_t  x = n1 ^ n2;
    int setBits = 0;
 
    while (x > 0) {
        setBits += x & 1;
        x >>= 1;
    }
 
    return setBits;
}

void printHistogram(int *hist, int n) {
      int i, j;
      for (i = 0; i < n; i++) {
      	printf("[%d] ", i);
      	for ( j = 0; j < hist[i]; ++j) {
      		printf("*");
      	}
      printf("\n");
   }
}

void hist(int * hist, int range) {
    int results[64];
    for (int i = 0; i < 64; ++i) {   
	    results[i] = 0;
    }

    for (int i = 0; i < 64; ++i) {   
      for(int j = 0; j < range; j++) {
         if ( hist[j] == i){
            results[i]++;
         }
      } 
    }
    printHistogram(results, 64) ;
}

int main(void)
{

	unsigned char data[64];

	for (int i = 0; i < 64; i++) {
		data[i] = rand() % 256 ;
	}

	// crc64 on this data
	uint64_t res = crc_64_we( data, 64 ) ;
	printf("CRC64 is %016lx \n",res);

	// use avalanche function
	uint64_t final = fmix64 ( res) ;
	printf("MRMR3 is %016lx \n",final);

	int h[64];	
	for (int i = 0; i < 64; i++) {
		uint64_t res2 = res ^ (1<<i);
		uint64_t final2 = fmix64 ( res2) ;
		int r = hammingDistance(final, final2);
		h[i] = r;
		printf("Hamming distance at pos %d is %d \n",i,r);
	}
	hist(h, 64) ;



    	return 0;
}
